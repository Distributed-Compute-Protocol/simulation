#!/usr/bin/env node

async function workFn(idx, a, b, c) {
  console.log(idx, a, b, c);
  progress();
  return idx * idx + a * a + b * b + c * c;
}

async function main() {
  const compute = require('dcp/compute');
  // Passing args a=1, b=2, c=3
  const job = compute.for([1.23, 2.34, 3.45, 1.23], workFn, [1, 2, 3]);

  job.public = { name: 'Simple Job' };
  job.public.description = process.env.USER + ' ' + new Date();
  job.on('complete',   (complete) => console.log(`Job completed ${complete.length} slices.`) );
  job.on('console', ({ message }) => console.log(message) );
  job.on('error',         (error) => console.log(error) );
  job.on('readystatechange', (rs) => console.log('Ready state:', rs) );
  job.on('accepted',           () => console.log('Job accepted:', job.id) );
  //job.on('status',       (status) => console.log('Status:', status) );
  //job.on('result',       (result) => console.log('Received result:', result) );

  let results = await job.localExec();
  console.log('Results are: ', results.values());

  // Validate results.
  results = results.values();
  const testResults = [ 15.5129, 19.4756, 25.9025, 15.5129 ];
  if (testResults.length !== results.length)
    throw new Error(`testResults.length(${testResults.length}) !==  results.length(${results.length})`); 
  for (let k = 0; k < testResults.length; k++) {
    if (Math.abs(testResults[k] - results[k]) > 1e-8)
      throw new Error(`${testResults[k]} is not sufficiently close to ${results[k]}`);
  }
}

const scheduler = 'https://scheduler.distributed.computer';
require('dcp-client')
  .init(scheduler)
  .then(main)
  .finally(() => setImmediate(process.exit));

