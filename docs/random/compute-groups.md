﻿# Compute Groups

A compute group is an association between a set of jobs and a set of workers. Workers and jobs can be in zero or many compute groups. One magic compute group, with joinKey: 'public', is public and auto-joined.  In order to "leave" it, users must explicitly opt-out.

## Record of Issue
Date        |  Author          | Change
----------- | ---------------- | ---------------------------------------------
May 2022    | Wes Garland      | Turned implementation document into genesis of specification

## Intended Audience
This document is intended for software developers working with DCP.  It is organized as a reference manual / functional specification; introductory documentation is in the DCP-Client document.

## Compute Group Service

### Executive Summary
- sub service of scheduler
- mediates all access to compute groups
	- except for cgadm tool which requires shell access to scheduler
- Legacy Implementation JIRA Link: https://kingsds.atlassian.net/browse/DCP-1412

### Database Note
There is a column in the database called `opaqueId` and a column in the database called `id`.  This is an implementation detail of the current Compute Group Service. The `id` column is a sequential identifier used by mysql and is *never sent on the wire*.  Where the user-facing API or the wire-line protocol use an `id` field, they refer to the `opaqueId` column of the database.

## Security Details
- A compute group has 
	- an owner
	- an id  (unique if specified)
	- an optional joinKey  (unique if specified)
	- an optional joinSecret (used to generate joinHash)
	- an optional joinAddress

In order to join a Compute Group, the job deployer or worker (*"member"*) must be able to 
1. identify the group, and 
2. satisfy the security principles.

### Identification
Compute Groups can be identified by either the `joinKey` or the `id`. It is possible to create groups with no `joinKey`; these groups must be identified by the `id`.

### Security Principles
* If a group that has joinKey and joinSecret, the member must know either the joinSecret or the joinHash, in order to join:
	* `{ joinKey, joinSecret }` or `{ joinKey, joinHash }`
	*  joinHash mechanism is preferred, and is used to protect the end user from easily giving away passwords they unwisely share across resources.
*  If the group has a joinAddress, the member must know the joinAddress, in order to join
	*  `{ id, joinAddress }`
	*  the join request payload must be signed using the keystore corresponding to this address, or
	*  the join request peerAddress (i.e. the requester's identity) must be this address
*  For security reasons, there is no feedback/error if we fail to join a group. Even knowledge of the existence of the group is useful to an attacker.

**Note Re joinSecret**: 
- `joinSecret` is never transmitted "over the wire", or stored in our database. 
- It is stored as `joinHash` and transmitted as `joinHashHash`.  
- The `joinHash` is made by adding the secret to either `joinKey` or (if not specified) the `joinAddress`. 
- The `joinHashHash` is made by hashing the `joinHash` with the current DCP session id as salt.

## Worker API extras for ComputeGroups
- `dcpConfig.worker.computeGroups`  object
- `dcpConfig.worker.leavePublicGroup` bool

## dcpConfig
- `dcpConfig.worker.computeGroups` is an object (usually an object representation of an Array) whose keys are ignored. This representation was chosen to make it easy to distribute static .REG files which patch `dcpConfig` on a Windows workstation.
- Each own property is an object which may have values `joinKey`, `joinSecret`, `joinHash`, `id`, `joinAddress`.
- One representation is:
	-  `{ 1:{ joinKey,joinHash }, 2:{ joinKey, joinSecret }, 3:{ id, joinAddress } }`

### Security Model
* The security model is that _anyone_ can join, provided they know:
	* `joinKey` and `joinSecret` or `joinHash` if `joinHash` specified in database
	* `id` and `joinAddress`

- There are 3 valid forms for joining a compute group;
	- `{ joinKey, joinSecret }`
	- `{ joinKey, joinHash }`
	- `( id, joinAddress }`  

## Compute API extras for ComputeGroups
- Legacy Implementation JIRA Link: https://kingsds.atlassian.net/browse/DCP-1418

### jobHandle.computeGroups
- This property identifies the initial compute group for a given job; these are the compute groups that it is placed into automatically during `jobHandle::exec()`. It has the same semantics as the values of `dcpConfig.worker.computeGroups` described in the Worker API section above. 
- It is an array of objects of the form:
	- `{ joinKey, joinSecret }`
	- `{ joinKey, joinHash }`
	- `( id, joinAddress }`  

When a jobHandle is instantiated, this value is populated with

```javascript
this.computeGroups = [{ joinKey: 'public', joinSecret: '' }];
```

- Replacing this property with a new array will cause `jobHandle::exec` to use the new array; failing to add the public compute group to a replacement array will result in a job which is not deployed to the public compute group.
- Changing or replacing this property after the job has been deployed has no effect.
 - During exec, the job is placed into the indicated compute group(s) via DCPv4 calls to the compute group management microservice.
 - It is not an error to deploy a job to no groups.
 - `jobHandle::exec()` will reject the returned promise if the user attempted to deploy the job to a compute group, but none of the supplied credentials matched.

### Examples
If a user wants to deploy work to a private group and not have it hit the public workers at all,

```let job = compute.for(set, func);
job.computeGroups = [{ joinKey: 'mySecret', joinSecret: 'myKeystoreLabel' }];
await job.exec();```



