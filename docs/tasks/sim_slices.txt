How to simulate slices.
We're testing the scheduler, so we need to simulate slices for the scheduler to send to simulated workers.
The work is simulated by a sleep function, because it doesn't matter what the work is.
It only matterns how often the requestTask sends slices to the worker.
---------------------------------------------------------------------------------------------------------
  // When things get real, we'll remove the console.log ....
  async function sleep(dt) {
    return new Promise(resolve => setTimeout(() => { console.log('seconds:', dt); resolve(); }, dt * 1000));
  }
  // Naive sleep -- we can do better
  async function naive_sleep(seconds, deviation) {
    seconds += (2 * Math.random() - 1) * deviation;
    return sleep(seconds);
  }
  // Given { mean, stddev }, sample from a dt0 ~ Gaussian(mean, stddev); dt = Math.exp(dt0)
  async function normal_sleep(mean, stddev) {
    dt0 ~ Gaussian(mean, stddev); // TODO: Implement this sampling
    dt = Math.exp(dt0);
    return seconds(dt);
  }
---------------------------------------------------------------------------------------------------------
A) Implement sampling dt0 from a Guassian.
Here's how to sample from Guassian(0, 1)
// Standard Normal variate using Box-Muller transform.
function randn_bm() {
    var u = 0, v = 0;
    while(u === 0) u = Math.random(); // Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    return Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
}
Then: dt0 = mean + stddev * randn_bm() is 1 sample from Guassian(mean, stddev).
--------------------------------------------------------------------------------------------------------
B) Put this code in the repository: git clone git@gitlab.com:Distributed-Compute-Protocol/simulation.git
Pick some names for the directory tree -- maybe something like:  simulation/guassian-sleep
Test guassian-sleep by making it into a DCP package and publishing it -- similar to Job5.js .
Put the test in the repository -- pick names.  Maybe something like: simulation/jobs/guassian-sleep .
Also check in a bash script for publishing the DCP package.
Make a normal MR and make me the Reviewer.
--------------------------------------------------------------------------------------------------------
C) The Box-Muller above is very naive and may be slightly wrong since I grabbed it from stackoverflow.
Read https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform and make it better if necessary.  
D) Do it with the Marsaglia polar method
E) Do it with the ziggurat algorithm
F) Check everything in.


