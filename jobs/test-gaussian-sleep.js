#!/usr/bin/env node
/**
 * @file    test-guassian-sleep.js
 *          This script tests gaussian-sleep.js, which is published as a DCP package
 * @author  Maria Kantardjian, maria@kingsds.network
 * @date    June 2022
 */

async function workFn(idx) {
    const { gaussian_sleep } = require('gaussian-sleep.js');
    console.log(`start slice${idx}...`);
    await gaussian_sleep(2, 2);
    progress();
    return idx * idx;
  }
  
  async function main() {
    const compute = require('dcp/compute');
    const job = compute.for(1, 15, workFn);
  
    job.requires(['gaussian-sleep/gaussian-sleep.js']);
  
    job.public = { name: 'Simple Job' };
    job.public.description = process.env.USER + ' ' + new Date();
    job.on('accepted',           () => console.log('Job accepted:', job.id) );
    job.on('complete',   (complete) => console.log(`Job completed ${complete.length} slices.`) );
    job.on('console', ({ message }) => console.log(message) );
    job.on('error',         (error) => console.log(error) );
    job.on('readystatechange', (rs) => console.log('Ready state:', rs) );
    job.on('status',       (status) => console.log('Status:', status) );
    job.on('result',       (result) => console.log('Received result:', result) );
  
    const results = await job.localExec();
  
    console.log('Results are: ', results.values());
  }
  
  const scheduler = 'https://scheduler.distributed.computer';
  require('dcp-client')
    .init(scheduler)
    .then(main)
    .finally(() => setImmediate(process.exit));