#!/usr/bin/env node
/**
 * @file    guassian-sleep.js
 *          This script is for simulating slices with sleep functions to send to simulated workers
 * @author  Maria Kantardjian, maria@kingsds.network
 * @date    June 2022
 */
'use strict';
module.declare([], function (require, exports, module) {
    async function sleep(dt) {
        return new Promise(resolve =>
            setTimeout(() => {
                console.log('seconds:', dt);
                resolve();
            }, dt * 1000));
    }

    async function naive_sleep(seconds, deviation) {
        seconds += (2 * Math.random() - 1) * deviation;
        return sleep(seconds)
    }


    /**
     * Applies box-muller transform to convert two sets of random numbers with uniform distribution into two sets of numbers with Gaussian(0,1) distribution
     * 
     * @returns one of the independent normally distributed random variables
     */

    function randn_bm() {
        var u = 0, v = 0;
        while (u === 0) u = Math.random(); // Converting [0,1) to (0,1)
        while (v === 0) v = Math.random();
        return Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);
    }


    /**
    * Applies Marsaglia polar method to generate a pair of independent standard normal random variables. This
    * method is more efficient than the box-muller method as it does not use trigonometric functions.
    * 
    * @returns one of the independent normally distributed random variables
    */

    function randn_mp() {
        var u, v, w;

        do {
            u = (2.0 * Math.random()) - 1.0; // Math.random() gives value on range [0, 1) but the Polar Form expects [-1, 1].
            v = (2.0 * Math.random()) - 1.0;
            w = Math.pow(u, 2) + Math.pow(v, 2);
        } while (w >= 1.0 || w == 0);

        return Math.sqrt((-2.0 * Math.log(w)) / w) * u;

    }

    /**
     * Applies the sleep function with duration sampled from normal distribution (resulting from box-muller transform, or marsaglia polar method)
     * with a specific mean and standard deviation. Exponentiate the result to exclude 0 and negative values.
     * 
     * @param {number} mean             
     * @param {number} stddev                 
     * 
     * @returns the result of the Promise after it has been resolved
     */
    exports.gaussian_sleep = async function gaussian_sleep(mean, stddev) {
        let dt0 = mean + stddev * randn_bm()
        let dt = Math.exp(dt0);
        return sleep(dt);
    }

})
