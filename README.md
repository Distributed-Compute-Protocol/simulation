# DCP Scheduler Simulation Project

## Description
Simulate 10,000 workers in order to stress test TD, the DCP Task Distributor.\
Eventually we'll also hook up RS (DCP Result Submitter) to the worker simulation.\
Refactor TD in an interface style to make it easy to override individual components.\
Research new scheduling algorithms.

## Authors and Support
Paul <paul@kingsds.network>\
Maria Kantardjian <maria@kingsds.network>

