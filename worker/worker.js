
/**
 *  @file       dcp-worker.js
 *              Standalone NodeJS DCP Worker
 *
 *  @author     Ryan Rossiter, ryan@kingsds.network
 *  @date       April 2020
 */
'use strict';

const process = require('process');
const os = require('os');
const fs = require('fs');

const DEFAULT_CORES = os.cpus().length - 1;
var worker, dcpConfig;

async function main () {
  process.on('unhandledRejection', unhandledRejectionHandler);

  await require('dcp-client').init(require(process.env.DCP_CONFIG || '../etc/dcp-config'));
  dcpConfig = require('dcp/dcp-config');
  require('../lib/check-scheduler-version').check();
  await startWorking(cliArgs);
}

// Preserve console.error, the dashboard replaces it with a custom logger
const logError = console.error;
main()
.then(() => process.exit(0))
.catch(e => {
  logError("Script failed:", e);
  process.exit(1);
});

function createManyAccounts(howMany, seed) {
  const workerAccounts = [];
  for (let k = 1; k < howMany; k++) {
    let sn = k.toString(16).padStart(4, '0');
    workerAccounts.push(seed.slice(0, seed.length - 4).concat(sn));
  }
  return workerAccounts;
}
const workerAccountSeed = '0x3C6197842bDdA47886Ed619E3F1ec8dD6447e516';
const workerAccounts = createManyAccounts(10, workerAccountSeed);
const identitySeed = '0x29F364e9b054A68A3EA1c5a0d72d23559CaD81F9';
const workerIdentities = createManyAccounts(10, identitySeed);

function constructWorker(_account, _identity) {
  const wallet = require('dcp/wallet');
  const DCPWorker = require('dcp/worker').Worker;
  const identityKeystore = await wallet.getId();
  const paymentAddress = new wallet.Address(_account);
  const identityAddress = new wallet.Address(_identity);
}
console.log('1', identityKeystore);
console.log('2', identityAddress);
console.log('3', paymentAddress);


function isHash(b) {
  return b.startsWith('eh1-');
}

async function startWorking(cliArgs) {
  const wallet = require('dcp/wallet');
  const DCPWorker = require('dcp/worker').Worker;
  const { startWorkerLogger } = require('../lib/startWorkerLogger');
  const identityKeystore = await wallet.getId();
  /** @type {string[]} */
  var dcpWorkerOptions;
  var paymentAddress;
  var sawOptions = {
    hostname: cliArgs.hostname,
    port:     cliArgs.port
  };

  if (cliArgs.paymentAddress) {
    paymentAddress = new wallet.Address(cliArgs.paymentAddress);
  } else {
    paymentAddress = (await wallet.get()).address;
  }

  dcpWorkerOptions = {
    paymentAddress,
    maxWorkingSandboxes: cliArgs.cores,
    priorityOnly:        cliArgs.priorityOnly,
    sandboxOptions: {
      SandboxConstructor: require('dcp-client/lib/standaloneWorker').workerFactory(sawOptions)
    },
    computeGroups: [], /* public group is implied */
    leavePublicGroup: cliArgs.leavePublicGroup
  };

  /* cliArgs.join is the list of compute groups to join */
  if (cliArgs.join && cliArgs.join.length)
  {
    dcpWorkerOptions.computeGroups = cliArgs.join
      .map((el) => {
        const [a,b] = el.split(',');
        return isHash(b) ? { joinKey: a, joinHash: b } : { joinKey: a, joinSecret: b };
      })                          /* Map cliArgs.join to give us [{ joinKey, joinSecret/joinHash }...] */
      .filter((el) => el.joinKey) /* Filter out entries with no joinKey */
    ;
    console.log( dcpWorkerOptions.computeGroups);
  }

  if (typeof dcpConfig.worker.unhandledRejectionCleanupTimeout !== 'undefined')
    unhandledRejectionHandler.timeout = dcpConfig.worker.unhandledRejectionCleanupTimeout;

  if (cliArgs.jobId) {
    dcpWorkerOptions.jobAddresses = cliArgs.jobId;
  }
  if (cliArgs.allowedOrigins) {
    dcpWorkerOptions.allowedOrigins = cliArgs.allowedOrigins;
  }

  worker = new DCPWorker(dcpWorkerOptions);

  if (process.env.TEST_HARNESS) {
    const { bindToTestHarness } = require('../lib/bindToTestHarness');
    bindToTestHarness(worker);
  }

  /* Delay log messages so that they appear in TUI window */
  setImmediate(() => {
    console.log(` * Starting DCP Worker`);
    console.log(` . Configured for scheduler ${dcpConfig.scheduler.location}`);
    console.log(` . Bank is ${dcpConfig.bank.location}`);
    console.log(` . Earned funds will be deposited in account ${paymentAddress}`);
    console.log(` . Identity is ${identityKeystore.address}`);

    function qty(amount, singular, plural) /* XXX i18n */
    {
      if (Array.isArray(amount))
        amount = amount.length;
      if (!plural)
        plural = singular + 's';
      if (!amount)
        return plural;
      if (amount == 1)
        return singular;
      return plural;
    }

    if (dcpWorkerOptions.jobAddresses)
      console.log(` * Processing only ${qty(dcpWorkerOptions.jobAddresses, 'job')}`, dcpWorkerOptions.jobAddresses.join(', '));
    if (dcpWorkerOptions.computeGroups.length)
      console.log(` * Joining compute ${qty(dcpWorkerOptions.computeGroups, 'group')}`, dcpWorkerOptions.computeGroups.map(el => el.joinKey).join(', '));
    if (dcpWorkerOptions.leavePublicGroup)
      console.log(' * Leaving the public compute group');
    console.log(' . ready');
  });

  /**
   * startWorkerLogger needs to be called before the worker is started so that
   * it can attach event listeners before the events fire, else UI events for
   * things such as progress will never get attached.
   *
   * setDefaultIdentityKeystore needs to be called before the logger because it
   * tries access the identity of the worker before it has started, i.e. where
   * it sets its identity, throwing an assertion error.
   *
   * FIXME(bryan-hoang): This is a fragile solution that is too coupled with the
   * implementation of the worker that should be addressed in Supervisor 2
   */
  await worker.supervisor.setDefaultIdentityKeystore();
  startWorkerLogger(worker, {
    verbose: cliArgs.verbose,
    outputMode: cliArgs.outputMode,
  });

  await worker.start();
  await new Promise(resolve => process.on('SIGQUIT', resolve));
  console.log('\n*** caught SIGQUIT; exiting...\n');
  await worker.stop(true);
}

/**
 * Unhandled rejection handler: __must not ever throw no matter what__.
 * If we hit an unhandled rejection, we are by definition no longer confident of our program state, meaning that
 * the worker must be restarted. This handler does its best to report the rejection and give the worker a few
 * seconds in which to attempt to return slices to the scheduler before it gives up completely.
 */
async function unhandledRejectionHandler (error) {
  let _worker = worker;

  if (!worker)
    return;
  else
    worker = false;

  try {
    let log = dcpConfig && dcpConfig.worker && dcpConfig.worker.unhandledRejectionLog;
    log = process.env.DCP_WORKER_UNHANDLED_REJECTION_LOG;
    if (log) {
      fs.appendFileSync(process.env.DCP_WORKER_UNHANDLED_REJECTION_LOG,
                        `${Date.now()}: ${error.message}\n${error.stack}\n\n`);
    }
  } catch(e) {};

  try {
    let screen = require('../lib/worker-loggers/dashboard').screen;

    if (screen) {
      screen.log(error.message + '\n' + error.stack);
      screen.destroy();
    } else {
      console.error('Unhandled rejection - preparing to exit:', error.message);
    }
  } catch(e) {};

  function bail(exitCode) {
    try {
      const util = require('util');
      process.stderr.write('\nWorker stop timeout; bailing due to earlier unhandled rejection:\n');
      process.stderr.write(util.inspect(error) + '\n');
    } catch(e) {
      console.error(error);
    }
    process.exit(exitCode || 3);
  }
  setTimeout(bail, 1000 * unhandledRejectionHandler.timeout);

  try {
    await _worker.stop(true);
  } catch(e) {
    console.log('Error during worker.stop:', e);
  }

  setImmediate(() => bail(33));
};
unhandledRejectionHandler.timeout = 5;

