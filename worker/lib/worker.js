
const process = require('process');
const os = require('os');
const fs = require('fs');

const DEFAULT_CORES = os.cpus().length - 1;
var worker, dcpConfig;

async function main () {
  process.on('unhandledRejection', unhandledRejectionHandler);

  await require('dcp-client').init(require(process.env.DCP_CONFIG || '../etc/dcp-config'));
  dcpConfig = require('dcp/dcp-config');
  require('../lib/check-scheduler-version').check();
}
